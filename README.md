# Rally-Connecté-Back-office publique **BACK-END**

Ce projet a été généré avec [Express Generator CLI].


Application développé lors d'un stage effectué dans l'entreprise[ e-creatures](https://www.e-creatures.com/) sur la periode Juillet-Aout 2019.
L'Application à ce stade en est à la version alpha 0.0.1 et reste fonctionnelle.

Développé par [Tony Lisambert.](https://www.linkedin.com/in/tony-lisambert/)