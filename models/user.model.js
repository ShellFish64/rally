module.exports = (sequelize, Sequelize) => {
	const user = sequelize.define('rallymanager', {
	  name: {
		  type: Sequelize.STRING,
		  allowNull: false
	  },
	  mail: {
		  type: Sequelize.STRING,
		  allowNull: false
	  },
	  password: {
		  type: Sequelize.STRING,
		  allowNull: false
	  },
	  resetToken: {
		type: Sequelize.JSON,
		allowNull: true
	  },
	  createdAt: {
		type: Sequelize.DATE,
		allowNull: true
	  },
	  updatedAt: {
		type: Sequelize.DATE,
		allowNull: true
	  },
	  tel: {
		type: Sequelize.INTEGER,
		allowNull: false
	}
	});
	
	return user;
}