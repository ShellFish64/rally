module.exports = (sequelize, Sequelize) => {
	const Team = sequelize.define('team', {
	  id_rally: {
		  type: Sequelize.INTEGER,
		  allowNull: false
	  },
	  name: {
		  type: Sequelize.STRING,
		  allowNull: false
	  },
	  mail: {
		type: Sequelize.STRING,
		allowNull: false
	  },
	  color: {
		type: Sequelize.STRING,
		allowNull: false
	  },
	  createdAt: {
		type: Sequelize.DATE,
		allowNull: true
	  },
	  updatedAt: {
		type: Sequelize.DATE,
		allowNull: true
	  },
	  deletedAt: {
		type: Sequelize.DATE,
		allowNull: true
	  }
	});
	
	return Team;
}