module.exports = (sequelize, Sequelize) => {
	const Waypoint = sequelize.define('rallye', {
	  /*id: {
		  primaryKey: true,
		  type: Sequelize.INTEGER,
		  allowNull: false
      },*/
      id_manager: {
        type: Sequelize.INTEGER,
        allowNull: false
	  },
	  name: {
        type: Sequelize.STRING,
        allowNull: false
	  },
	  date_expiration: {
        type: Sequelize.DATE,
        allowNull: false
	  },
	  city: {
        type: Sequelize.STRING,
        allowNull: true
	  },
      lat: {
		type: Sequelize.FLOAT,
		allowNull: true
      },
      lng: {
		type: Sequelize.FLOAT,
		allowNull: true
	  },
	  prepa: {
		  type: Sequelize.JSON,
		  allowNull: true
	  },
	  playable: {
		type: Sequelize.JSON,
		allowNull: true
	  },
      hashLink: {
		type: Sequelize.STRING,
		allowNull: true
	  },
	  deletedAt: {
		type: Sequelize.DATE,
		allowNull: true
	  }
	});
	
	return Waypoint;
}