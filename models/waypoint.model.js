module.exports = (sequelize, Sequelize) => {
	const Waypoint = sequelize.define('waypoint', {
	  id_rally: {
		  type: Sequelize.INTEGER,
		  allowNull: false
	  },
	  title: {
		type: Sequelize.STRING,
		allowNull: true
	  },
	  lat: {
		type: Sequelize.FLOAT,
		allowNull: false
      },
      lng: {
		type: Sequelize.FLOAT,
		allowNull: false
      },
      iconePng: {
		type: Sequelize.TEXT,
		allowNull: true
      },
      iconeSvg: {
		type: Sequelize.TEXT,
		allowNull: true
      },
      photo: {
		type: Sequelize.TEXT,
		allowNull: true
      },
      question: {
		type: Sequelize.TEXT,
		allowNull: false
      },
      reponse1: {
		type: Sequelize.TEXT,
		allowNull: false
      },
      reponse2: {
		type: Sequelize.TEXT,
		allowNull: false
      },
      reponse3: {
		type: Sequelize.TEXT,
		allowNull: false
      },
      valrep1: {
		type: Sequelize.BOOLEAN,
		allowNull: false
      },
      valrep2: {
		type: Sequelize.BOOLEAN,
		allowNull: false
      },
      valrep3: {
		type: Sequelize.BOOLEAN,
		allowNull: false
      },
      deletedAt: {
          type: Sequelize.DATE,
          allowNull: true
      }
	});
	
	return Waypoint;
}