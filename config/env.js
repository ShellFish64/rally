const env = {
    database: 'rally',
    username: 'root',
    password: 'root',
    host: 'localhost',
    port: 3306,
    dialect: 'mysql',
    /*database: 'rally',
    username: 'root',
    password: 'root',
    host: 'localhost',*/
    /*database: 'rallyebd',
    username: 'tony',
    password: 'Y0l0swag',
    host: 'pm641-001.dbaas.ovh.net',
    port: 35236,
    dialect: 'mysql',*/
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};
module.exports = env;