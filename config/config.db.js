const env = require('./env');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(env.database, env.username, env.password, {
    host: env.host,
    dialect: env.dialect,
    //operatorsAliases: false,   
    pool: {
      max: env.max,
      min: env.pool.min,
      acquire: env.pool.acquire,
      idle: env.pool.idle
    },
    dialectOptions: {
      socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock'
    },
    define: {
      paranoid: false
  }
});
//Sequelize environement attribution
//We can now establish models on our database entities...
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
//For exports entiere sequelized obj


db.user = require('../models/user.model.js')(sequelize, Sequelize);
db.waypoint = require('../models/waypoint.model.js')(sequelize, Sequelize);
db.team = require('../models/team.model.js')(sequelize, Sequelize);
db.rally = require('../models/rally.model.js')(sequelize, Sequelize);
//Models are here..

module.exports = db;
