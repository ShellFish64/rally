const express = require('express');
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
//Require routes
const apiAuth = require('./routes/api-auth-router/router');
const crudRally = require('./routes/rally/router');

const app = express();
const PORT = process.env.PORT || 8010;

//MiddleWares
app.use(cors());
app.use('/static/upload/upload_img', express.static(path.join(__dirname, 'upload_img')));
app.use('/static/upload/upload_ico', express.static(path.join(__dirname, 'upload_ico')));
//app.use('/upload', express.static(path.join(__dirname, 'upload_img')));
app.use(bodyParser.urlencoded({limit: '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));
//Routes
app.use('/api/auth/', apiAuth);
app.use('/rally/', crudRally);

app.listen(PORT, function() {
  console.log('server is running at port : ', PORT)
});

module.exports = app;
 