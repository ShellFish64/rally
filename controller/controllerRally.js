const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const mysql = require('mysql');
const db = require('../config/config.db.js');
const config = require('../config/config');
const path = require('path');
const fs = require('fs');
const Datauri = require('datauri');
const Sequelize = require('Sequelize');
//const dataUri = require('')
//Get the sequelized DB config (orm)
//Get the token secret config

const upload = require('../routes/rally/uploader');

const User = db.user;
const Waypoint = db.waypoint;
const Rally = db.rally;
const Team = db.team;

const datauri = new Datauri();
//datauri.pipe(process.stdout);

exports.getRallys = (req, res) => {
    Rally.findAll({
        where: {
            deletedAt: null,
            id_manager: req.userId,
        }
    }).then(result => {
        console.log(result);
        res.status(200).send({
            rallys: result
        })
    })
}


exports.delete = (req, res, next) => {
    console.log(req.params.id);
    Rally.findOne({
        where: {
            id_manager: req.userId,
            deletedAt: null,
            hashLink: req.params.id
        }
    }).then(result => {
        console.log(result);
        if(result === null){
            res.status(403).send();
            res.end();
            return;
        }
        //result[0]
        result.deletedAt = Date.now();
        result.save({fields: ['deletedAt']}).then(() => {
            // title will now be dated
            res.status(200).send();
        })
    })
}

//Deleting request via sequelize.

exports.edit = (req, res, next) => {
    console.log('ediiiiiiiiiiit');
    console.log(req.params);
    const Op = Sequelize.Op;
    const rallyInfos = req.body.rally[0];
    const rallyWpts = req.body.rally[1];
    const date = new Date(rallyInfos.expiration.year,rallyInfos.expiration.month,rallyInfos.expiration.day);

    //Find the rally...
    Rally.findOne({
        where: {
            id_manager: req.userId,
            deletedAt: null,
            playable: {[Op.ne]: null},
            hashLink: req.params.id,
        }
    }).then(rally => {
        if(rally === null){
            res.status(403).send();
            res.end();
            return;
        }
        //then upating raLLyDB infos
        rally.name = rallyInfos.name;
        rally.date_expiration = date;
        rally.updatedAt = Date.now();
        rally.playable = req.body.rally;
        rally.prepa = req.body.rally;
        //Prepa is very important, because we request him even if we are on playable...
        //Se we absolutly must update him.

        //Then saving rally fields:
        rally.save({fields: ['updatedAt']});
        rally.save({fields: ['date_expiration']});
        rally.save({fields: ['name']});
        rally.save({fields: ['playable']});
        rally.save({fields: ['prepa']});
        res.status(200).send();
    });

}
//Editing request via sequelize

exports.getFullRally = (req, res, next) => {
    Rally.findOne({
        where: {
            id_manager: req.userId,
            hashLink: req.params.id,
            deletedAt: null
        }
    }).then(rally => {
        if(rally === null){
            res.status(403).send();
            res.end();
            return;
        }
        /*if(rally.playable !== null){
            res.status(200).send({
                rally: rally.playable[0],
                waypoints: rally.playable[1],
                allInfos: rally
            });
            res.end();
            return;
        } semble ne servir a rien */
        res.status(200).send({
            rally: rally.prepa[0],
            waypoints: rally.prepa[1],
            allInfos: rally
        });
    });
}
