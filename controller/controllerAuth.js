const db = require('../config/config.db.js');
const config = require('../config/config');
//Get the sequelized DB config (orm)
//Get the token secret config
const User = db.user;

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const mailer = require('../routes/api-auth-router/mailer');
const mysql = require('mysql');
const Sequelize = require('sequelize');

exports.signUp = (req,res) => {
    // Save User to Database
    console.log("Processing func -> SignUp");
    console.log(req.body);
    User.create({
        name: req.body.name,
		tel: req.body.tel,
		mail: req.body.mail,
		password: bcrypt.hashSync(req.body.password, 8)
    }).then(user => {
        res.status(200).send();
    }).catch(err => {
        res.status(500).send("Fail! Error -> " + err);
    });
}

exports.signIn = (req,res) => {
    // Generate a token if login is ok
    console.log(req.body);
    User.findOne({
        where: {
            mail: req.body.mail
        }
    }).then(user => {
        if(!user){
            res.status(404).send('User not found');
        }
        //Chiffrage du mot de passe en abse de données
        let passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
        if(!passwordIsValid){
            return res.status(401).send({ auth: false, accessToken: null, reason: "Invalid Password!" });
        }
        //Attribution du token: id, name
        let token = jwt.sign( { id: user.id, name: user.name }, config.secret, {expiresIn: 186400});
        res.status(200).send({ auth: true, accessToken: token });
    }).catch(err => {
        res.status(500).send('ErrorSignIn -> ' + err);
    });
}

exports.checkMail = (req, res, next) => {
    console.log(req.body);
    User.findOne({ 
        where: {
            mail: req.body.mail
        }
    }).then(user => {
        console.log(user);
        if (!user) {
           return res.status(404).send();
        }
        req.user = user;
        next(user);
    }).catch(err => {
        console.log(err);
        res.status(500).send('Error ->' + err);
    })
}

exports.mail = (user, req, res, next) => {
    console.log('in mail');
    let token = jwt.sign({ id: req.user.id }, config.secret, {expiresIn: 3600});
        user.resetToken = token;
        user.save({fields: ['resetToken']});
        mailer.send({
            from: 'contact.rallyconnecte@gmail.com',
            to: req.user.mail,
            subject: 'Mot de passe oublié',
            text: 'Voici un lien valable 1 heure qui vous permettant de réinitialiser votre mot de passe: ' + 'http://localhost:4200' + '/reset/' + token
        }, (err, info) => {
            if (err) {
                console.log(err);
                return res.status(500).send();
            } else {
                console.log(info.response);
                res.status(200).send();
            }
        });
}

exports.updatePass = (token, req, res, next) => {
    
    const Op = Sequelize.Op;

    if (req.body.password) {
        User.findOne({
            where: {
                resetToken: {[Op.like]: token}
            }
        }).then(user => {
            console.log(user);
            user.password = bcrypt.hashSync(req.body.password, 8);
            user.save({fields: ['password']});
            res.status(200).send();
        })
    } else { 
        return res.status(404).send();
    }
}

exports.userContent = (req,res,next) => {
    //fetch attributes from table where id = decoded id 
    //(look at router.js, we call authJwT.verifyToken to decode id from token then put it on req)
    console.log('checkingToken')
    User.findAll({
        attributes: ['name', 'id', 'mail'],
        where: {
            id: req.userId
        },
    }).then(user => {
        res.status(200).json({
            "iss": "rally.connecte.com",
            "id": user.id,
            "name": user.name,
            "desc": "desc",
        })
    }).catch(err => {
		res.status(500).json({
			"description": "Can not access User Page",
			"error": err
        });
    });
}

