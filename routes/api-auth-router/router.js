const router = require('express').Router();
const bodyParser = require('body-parser');
const controller = require('../../controller/controllerAuth');
const cors = require('cors');


//Middlewares
router.use(bodyParser.json());
//Forms are now JSON
const authJwT = require('./verifyJwT');
//Get the token checker middleware

const verifySignUp = require('./verifySignUp');
//Get the sign up checker / sign up process  middleware

router.post('/signup', verifySignUp.checkDuplicateUserNameOrEmail, controller.signUp);
router.post('/signup', (req,res,next) => {
    res.redirect('/dashboard');
});
router.post('/signin', controller.signIn);
/*router.post('/signin', (req,res,next) => {
    res.redirect('/signin');
});*/
router.get('/dashboard', authJwT.verifyToken, (req,res,next) => {
    controller.userContent(req,res, next);
});
router.get('/getNameOfUser', authJwT.verifyToken, (req,res,next) => {
    controller.userContent(req,res, next);
});
router.get('/checkToken', authJwT.simpleVerify);
router.post('/pwd_reset', controller.checkMail, controller.mail);
router.get('/reset/:token', authJwT.verifyTokenReset);
router.post('/reset/:token', authJwT.verifyTokenReset, controller.updatePass);
//router.post('/reset/:token')



module.exports = router;