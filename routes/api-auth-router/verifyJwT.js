const config = require('../../config/config');
const jwt = require('jsonwebtoken');

verifyToken = (req,res, next) => {
    let token = req.headers['x-access-token'];
    console.log(token);
    //get x-access-token header
    console.log('action');
    console.log(req.body);
    if(!token){
        console.log('send err')
        return res.status(403).send({ 
			auth: false, message: 'No token provided.' 
		});
    }
    //Check: if x-access-token header exists
    //If header exists:
    jwt.verify(token, config.secret, (err, decoded) => {
        console.log('verifying..')
        if (err){
			return res.status(500).send({ 
					auth: false, 
                    message: 'Fail to Authentication. Error -> ' + err ,
                    prvidedToken: token
				});
        }
    //Try to decode it trough config.secret then put id on req, then : next middleware.
        req.userId = decoded.id;
        console.log('passed');
        next();
    })
}

simpleVerify = (req,res,next) => {
    console.log('requested');
    let token = req.headers['x-access-token'];
    if(!token) {
        return res.status(403).send({ 
			auth: false, message: 'No token provided.' 
		});
    }
    jwt.verify(token, config.secret, (err, decoded) => {
        console.log('verifying..')
        if (err){
			return res.status(500).send({ 
					auth: false, 
                    message: 'Fail to Authentication. Error -> ' + err ,
                    prvidedToken: token
				});
        }
    //Try to decode it trough config.secret then put id on req, then : next middleware.
        res.status(200).send();
        console.log('passed');
        next();
    })
}

verifyTokenReset = (req, res, next) => {

    let token = req.params.token;

    if (!token) {
        res.status(400).send();
        return;
    }

    jwt.verify(token, config.secret, (err, decoded) => {

        if (err){
            console.log(err);
			return res.status(500).send(
                {
                    message: 'Erreur, votre jeton n\'est peut-etre plus utilisable...'
                }
            );
        }
        console.log('token is okkk : next tomorow tony!');
        
        if (req.method === 'GET') {
            res.status(200).send();
        }
        next(token);

    });
}
//Password reset: verifying the token into url

const verifyJwT = {};
verifyJwT.verifyToken = verifyToken;
verifyJwT.simpleVerify = simpleVerify;
verifyJwT.verifyTokenReset = verifyTokenReset;
module.exports = verifyJwT;