const mailer = require('nodemailer');


const transporter = mailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
        user: 'contact.rallyconnecte@gmail.com',
        pass: 'rallyrally'
    }
})

exports.send = (mailOptions, callback) => {
    transporter.sendMail(mailOptions, (err, info) => {
        callback(err, info);
    })
}

