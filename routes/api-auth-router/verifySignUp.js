const db = require('../../config/config.db.js');
const config = require('../../config/config');
const User = db.user;

checkDuplicateUserNameOrEmail = (req, res, next) => {
    //check if mail already used
    console.log(req.body)
    User.findOne({
        attributes: ['name'],
        where: {
            mail: req.body.mail
        }
    }).then(user => {
        if(user){
            res.status(400).send('Fail-> mail déja pris!');
            return;
        }
        //check if tel already used
        User.findOne({
            attributes: ['name'],
            where: {
                tel: req.body.tel 
            }
        }).then(user => {
            if(user){
                res.status(400).send('Fail-> tel déja pris!');
            }
            next();
        });
    });
}

const signUpVerify = {};
signUpVerify.checkDuplicateUserNameOrEmail = checkDuplicateUserNameOrEmail;

module.exports = signUpVerify;