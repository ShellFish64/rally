const express = require('express');
const router = express();
const bodyParser = require('body-parser');
const controller = require('../../controller/controllerAuth');
const cors = require('cors');
const randomstring = require("randomstring");
//const util = require('util');
//const path = require('path');
const authJwT = require('../api-auth-router/verifyJwT');
const upload = require('./uploader');
const dataToBlob = require('./dataURItoBlob');
const fs = require('fs');
const Sequelize = require('Sequelize');
const typeChecker = require('./typeChecker');
//controller rally
const controllerRally = require('../../controller/controllerRally');

//config files
const db = require('../../config/config.db.js');
const config = require('../../config/config');

//Get the sequelized DB config (orm)
//Get the token secret config
const User = db.user;
const Waypoint = db.waypoint;
const Rally = db.rally;
const Team = db.team;

router.use(cors());
router.use(bodyParser.json());
//Forms are now JSON
router.use((req,res,next) => {
    console.log('hello');
    req.errors = {};
    next();
});
//For errors storing


//Creation mode:
router.post('/creation', authJwT.verifyToken);
//First: check if guy is connected by getting token..

router.post('/creation', (req,res, next) => {
    let checker;
    //We are establishing a CHECKING SERIES wich are the ONLY WAYS for each of them
    //If even one of theses check conditions are not respected: We abort the request by passing checker to false!.

    console.log(req.body);
    if(checker !== false) { 
        typeChecker.iFThereIsRally(req.body.rally, () => {
            console.log('plup');
                res.status(403).send({ 
                    message: 'Impossible de donner suite a votre requete: wrong files'
                });
                res.end();
        });
    } else {
        return;
    }
    //Request must ONLY contain rally and rally must be an Array, else: we don't talks anymore!

    if(checker !== false) { 
        checker = typeChecker.iFiTSeemsToBeRallyType(req.body.rally, () => {
            console.log('plup');
            res.status(403).send({ 
                message: 'Impossible de donner suite a votre requete: wrong files'
            });
            return false;
        })
        console.log(checker);
    } else {
        return;
    }
    //Array own 2 elements : array of all waypoints and rally informations, else: we don't talk anymore!  
    //Array of waypoints Must contain 1 elements ore MORE
    //Then conditionnaly creating Rally, waypoints and team for this rally

    const rallyInfos = req.body.rally[0];
    const rallyWpts = req.body.rally[1];
    console.log(checker);
    if(checker !== false) {
        checker = typeChecker.iFThereIsRallyName(rallyInfos, () => {
            console.log('plup');
            res.status(403).send({ 
                message: 'Impossible de donner suite a votre requete: wrong files'
            });
            return false;
        })
    } else {
        return;
    }

    if(checker !== false) {
        chceker = typeChecker.iFThereIsRallyExp(rallyInfos, () => {
            console.log('plup');
            res.status(403).send({ 
                message: 'Impossible de donner suite a votre requete: wrong files'
            });
            return false;
        })
    } else {
        return;
    }

    if(checker !== false) {
        checker = typeChecker.iFSeemsToBeExpType(rallyInfos, ()=> {
            console.log('plup');
            res.status(403).send({ 
                message: 'Impossible de donner suite a votre requete: wrong files'
            });
            return false;
        })
    } else {
        return;
    }
   
    //Somes checking ! ..

    if(checker !== false) {
        checker = typeChecker.iFThereIsRallyTeams(rallyInfos, () => {
            console.log('plup');
            res.status(403).send({ 
                message: 'Impossible de donner suite a votre requete: wrong files'
            });
            return false;
        })
    } else {
        return;
    }
    //Somes checking ! ..
    
    const date = new Date(rallyInfos.expiration.year,rallyInfos.expiration.month,rallyInfos.expiration.day);
    const hash = randomstring.generate(7);
    //W'll later need theses variables for inserting into DB !
    
    req.body.rally[2] !== undefined ? req.body.rally[2] = undefined: null;
    //Will be rectified later.. if we let thats it'll generate an error for max Json depth's.

    if(checker !== false) { 
        Rally.findOne({
            where: {
                id_manager: req.userId,
                name: rallyInfos.name,
                playable: null,
                //Rajouter verification du hashlink et rajouter infos db du rally
                hashLink: req.body.infos.hashLink, 
                deletedAt: null,
            }
        })
        .then(result => {
            console.log(result);
            if(result !== null){
                result.name = rallyInfos.name,
                result.prepa = req.body.rally,
                result.playable = req.body.rally,
                date_expiration = date,
                result.updatedAt = Date.now();
                result.save({fields: ['name']});
                result.save({fields: ['updatedAt']});
                result.save({fields: ['date_expiration']});
                result.save({fields: ['prepa']});
                result.save({fields: ['playable']});

                res.status(200).send();
                res.send();
                return;
            }
            //We'll allways have hashLink into infos since client get infos after step1...
            //So we always update it and turn it to playable by inserting field..

        });

    } else {
        return;
    }

});

router.get('/getRallys', authJwT.verifyToken);
//Get user id by token checking
router.get('/getRallys', controllerRally.getRallys);

router.delete('/delete/:id', authJwT.verifyToken);
router.delete('/delete/:id', controllerRally.delete);

router.get('/getFullRally/:id', authJwT.verifyToken);
router.get('/getFullRally/:id', controllerRally.getFullRally);

router.put('/edit/:id', authJwT.verifyToken);
router.put('/edit/:id', controllerRally.edit);

router.post('/storage', authJwT.verifyToken);
router.post('/storage', (req,res,next) => {
//Here we got a notion of "STORAGE"..
//Explainations: STORAGE is a PERSISTENCE STRATEGY wich can be used  on PLAYABLES(EDIT MODE)(/:id/edit-step/:id);
//Or as well on PREPA(CREATION MODE and EDITIING A CREATION)(creation-step/:id);
//So we are able to offer a PERSISTENECE by this way on: UNLOAD/FREFRESH CLIENT PAGE,NEXT/PREV STEP...
console.log(req.body);
let rallyInfos;
let rallyWpts

    let checker = typeChecker.iFThereIsRally(req.body.rally, () => {
        console.log('plup');
        res.status(403).send({ 
            message: 'Impossible de donner suite a votre requete: wrong files'
        });
        res.end();
        return false;
    });

    if(checker === false){
        return;
    }

    //only for PREPA EDITING inter-steps storage
    //So we can have RALLY INFOS on EDITING PREPA as not have RALLY INFOS on CREATION PREPA
    //Request must ONLY contain rally and rally must be an Array, else: we don't talks anymore!
    
    if(!Array.isArray(req.body.rally[0]) && Array.isArray(req.body.rally[1]) && req.body.infos === undefined || 
    !Array.isArray(req.body.rally[0]) && Array.isArray(req.body.rally[1]) && req.body.infos && (req.body.infos.playable === null || req.body.infos.playable === undefined)) {

        req.body.rally[2] !== undefined ? req.body.rally[2] = undefined: null;
        //Will be rectified later.. if we let thats it'll generate an error for max Json depth's.
        
        rallyInfos = req.body.rally[0];
        rallyWpts = req.body.rally[1];
        const date = new Date(rallyInfos.expiration.year,rallyInfos.expiration.month,rallyInfos.expiration.day);
        const hash = randomstring.generate(7);

        //CREATION MODE :

        if(req.body.infos === undefined){
                console.log('creation');
            Rally.create({
                id_manager: req.userId,
                name: rallyInfos.name,
                date_expiration: date,
                hashLink: hash,
                prepa: req.body.rally
            })
            .then(result => {
                console.log(result);
                if(result !== null){
                    res.status(201).send({
                        rallyCreated: result,
                        status: 201
                    });
                    res.send();
                    return;
                }
            });
    }
    
        //fetch rally WICH IS NOT finalized
        //If we got it we just update the current prepa
        //Else creating new prepa :)

        
        if(req.body.infos && req.body.infos.hashLink !== undefined){
            Rally.findOne({
                where: {
                    id_manager: req.userId,
                    hashLink: req.body.infos.hashLink,
                    playable: null,
                    deletedAt: null,
                }
            })
            .then(result => {
                console.log(result);
                if(result !== null){
                    req.body.rally[2] = undefined;
                    console.log(req.body.rally);
                    console.log(req.body.rally);
                    result.prepa = req.body.rally;
                    result.name = rallyInfos.name;
                    result.date_expiration = date;
                    result.updatedAt = Date.now();
                    result.save({fields: ['updatedAt']});
                    result.save({fields: ['prepa']});
                    result.save({fields: ['name']});
                    result.save({fields: ['date_expiration']});
                    res.status(200).send({
                        status: 200
                    });
                    res.send();
                    return;
                }
            });
        }
    }
    //only for PREPA Editing
    //Request must ONLY contain rally and rally must be an Array, else: we don't talks anymore!
    
    if(!Array.isArray(req.body.rally[0]) && Array.isArray(req.body.rally[1]) && req.body.infos 
    && (req.body.infos.playable !== null && req.body.infos.playable !== undefined)) {
        req.body.rally[2] !== undefined ? req.body.rally[2] = undefined: null;
        rallyInfos = req.body.rally[0];
        rallyWpts = req.body.rally[1];
        const date = new Date(rallyInfos.expiration.year,rallyInfos.expiration.month,rallyInfos.expiration.day);
        const hash = randomstring.generate(7);
        const Op = Sequelize.Op;
        console.log(req.body.rally);
        console.log(rallyInfos);
        //fetch rally WICH IS finalized
        //Just update the current playable
        Rally.findOne({
            where: {
                id_manager: req.userId,
                name: rallyInfos.name,
                //hashLink: req.body.infos.hashLink,
                playable: {[Op.ne]: null},
                deletedAt: null,
            }
        })
        .then(result => {
            console.log(result);
            console.log(req.body);
            if(result !== null){
                result.playable = req.body.rally;
                result.prepa = req.body.rally;
                result.updatedAt = Date.now();
                result.save({fields: ['updatedAt']});
                result.save({fields: ['playable']});
                result.save({fields: ['prepa']});

                res.status(200).send();
                res.send();
                return;
            }
        });
    }
    //only for PLAYABLE Editing inter-steps storage
    //Request must ONLY contain rally and rally must be an Array, else: we don't talks anymore!
});

//router.post('/upload', authJwT.verifyToken)
router.post('/upload', upload.uploader)
module.exports = router