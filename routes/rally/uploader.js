const md5 = require('md5');
const fs = require('fs');
const path = require('path');

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const dataToBlob = require('./dataURItoBlob');
//to datauri -> Blob

exports.uploader = (req, res, next) => {
    
    // Write the file on server from DataURI
    //Then checking upload to send request

    let writer = (fileBlob, pathToPlace, extansion, title) => {

        //Check if file has been correctly uploaded bfore sending response contenting url of uploeaded file
        //Instead of using callback on the writestram we manually check 
        //if the entiere path of hypothetic file is existing.
        let checkUploadedFile = () => {
            console.log('go');
            fs.open(path.join(__dirname, pathToPlace + title + '.' + extansion),'r',(err) => {
                if (err) {
                    req.errors['uploadErr'] =  "Erreur au moment de l'upload du média";
                    if (err.code === 'ENOENT') {
                        req.errors['uploadErr'] =  "Erreur CRITIQUE au moment de l'upload du média";
                    }
                    console.log(req.errors['uploadErr']);               
                    res.status(500).send({
                        errorServer: req.errors['uploadErr']
                    });
                    return;
                }
                res.status(200).send({
                    media: title + '.' + extansion
                });
            })
        }

        fs.createWriteStream(path.join(__dirname, pathToPlace + title + '.' + extansion)).write(fileBlob, (err) => {
            //We shall bring error to user after uploading so put it into req.errors..
            if(err) {
                req.errors['uploadErr'] = "Erreur au moment de l'upload du média";
                console.log(req.errors);
                console.log(err);
                res.status(500).send({
                    err: req.errors['uploadErr']
                })
                return;
            }
            
            //Upload file then check if is correctly uploaded
            checkUploadedFile();
        });    
    }
    
    //Make unique title for pictures...
    let titleMaker = () => {
        return md5(Date.now());
    }
    //get type of blob parsing string...
    let getTypeofImage = (blob) => {
        if(blob.type) {
            typeImage = blob.type;
            return typeImage = typeImage.slice(typeImage.indexOf("/") + 1);
        }
    }


    console.log(req.body);
    console.log(req.body.image);
    let svg;
    let png;
    //icones
    let image;
    //image for Wpt

    if(req.body.iconeSvg) {
        const iconeSvg = dataToBlob.dataURItoBlob(req.body.iconeSvg);
        console.log('svg call');
        title = titleMaker();
        svg = writer(iconeSvg.buffer, '../../upload_ico/', 'svg', title);
    }
    if(req.body.iconePng) {
        const iconePng = dataToBlob.dataURItoBlob(req.body.iconePng);
        console.log('png call');
        title = titleMaker();
        png = writer(iconePng.buffer, '../../upload_ico/', 'png', title);
    }
    if(req.body.image) {
        const imageBloblFromUri = dataToBlob.dataURItoBlob(req.body.image);
        console.log('image call');
        title = titleMaker();
        image = writer(imageBloblFromUri.buffer, '../../upload_img/', getTypeofImage(imageBloblFromUri), title);
    }

}



