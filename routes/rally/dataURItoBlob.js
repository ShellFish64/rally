const atob = require('atob');
const Blob = require('node-blob');

function dataURItoBlob(dataURI) {
    let byteStr;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteStr = atob(dataURI.split(',')[1]);
    else
        byteStr = unescape(dataURI.split(',')[1]);

    const mimeStr = dataURI.split(',')[0].split(':')[1].split(';')[0];

    const arr= new Uint8Array(byteStr.length);
    for (var i = 0; i < byteStr.length; i++) {
        arr[i] = byteStr.charCodeAt(i);
    }

    return new Blob([arr], {type:mimeStr});
}

let dataToBlob = {};
dataToBlob.dataURItoBlob = dataURItoBlob;
module.exports = dataToBlob;