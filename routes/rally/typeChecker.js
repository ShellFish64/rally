


exports.iFThereIsRally = (target, callBack, next) => {
    if( !(target !== undefined && Array.isArray(target) && target.length === 2 || target !== undefined && Array.isArray(target) && target.length === 3) ) {

        if(typeof(callBack) === 'function'){
            return callBack();
        }
    }
}

exports.iFiTSeemsToBeRallyType = (target, callBack, next) => {
    if( !(!Array.isArray(target[0]) && Array.isArray(target[1]) && target[1].length > 0) ) {

        if(typeof(callBack) === 'function'){
            return callBack();
        }
    }
}

exports.iFThereIsRallyName = (target, callBack, next) => {
    if( !(target.name !== null && target.name !== undefined && typeof(target.name === 'sring') && target.name.length > 3) ) {

        if(typeof(callBack) === 'function'){
            return callBack();
        }
    }
}

exports.iFThereIsRallyExp = (target, callBack, next) => {
    if(
        !(target.expiration !== null && target.expiration !== undefined 
        && target.expiration.day !== null&& target.expiration.day !== undefined
        && target.expiration.year !== null && target.expiration.year !== undefined 
        && target.expiration.month !== null && target.expiration.month !== undefined)
    ){
        if(typeof(callBack) === 'function'){
           return  callBack();
        }
    }
}

exports.iFSeemsToBeExpType = (target, callBack, next) => {
    if( !(typeof(target.expiration.day) === 'number' && typeof(target.expiration.year) === 'number' && typeof(target.expiration.month) === 'number')){

        if(typeof(callBack) === 'function'){
            return callBack();
        }
    }
}

exports.iFThereIsRallyTeams = (target, callBack, next) => {
    if( !(target.teams !== null && target.teams !== undefined && Array.isArray(target.teams) && target.teams.length > 0) ) {

        if(typeof(callBack) === 'function'){
            return callBack();
        }
    }
}




