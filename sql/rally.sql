-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le :  mer. 02 oct. 2019 à 15:23
-- Version du serveur :  5.7.25
-- Version de PHP :  7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `rally`
--

-- --------------------------------------------------------

--
-- Structure de la table `rallyes`
--

CREATE TABLE `rallyes` (
  `id` int(11) NOT NULL,
  `id_manager` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_expiration` date NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `hashLink` varchar(255) NOT NULL,
  `lat` float(10,6) DEFAULT NULL,
  `lng` float(10,6) DEFAULT NULL,
  `prepa` json DEFAULT NULL,
  `playable` json DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `deletedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `rallyes`
--

INSERT INTO `rallyes` (`id`, `id_manager`, `name`, `date_expiration`, `city`, `hashLink`, `lat`, `lng`, `prepa`, `playable`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(67, 5, 'Ecreatures Bidart', '2019-08-10', NULL, 'YAdBXGw', NULL, NULL, '[{\"name\": \"Ecreatures Bidart\", \"zoom\": 6, \"teams\": [{\"mail\": \"mail@mail.com\", \"name\": \"Test\", \"color\": \"#5DD529\"}], \"viewport\": {\"lat\": 51.4378319939697, \"lng\": 6.642769950632759}, \"expiration\": {\"day\": 11, \"year\": 2019, \"month\": 7}}, [{\"image\": {\"url\": \"07af678c18438f69d40704f7cafa0496.jpeg\"}, \"quizz\": {\"ask\": \"ask 1 ?\", \"answer1\": {\"value\": false, \"answerBody\": \"a\"}, \"answer2\": {\"value\": true, \"answerBody\": \"b\"}, \"answer3\": {\"value\": false, \"answerBody\": \"c\"}}, \"coords\": {\"lat\": 51.411956689602626, \"lng\": 5.449693035156315}, \"iconePng\": {\"url\": \"3e222d4aae19c872b14766270707a372.png\"}, \"iconeSvg\": {\"url\": null}}, {\"image\": {\"url\": null}, \"quizz\": {\"ask\": \"test\", \"answer1\": {\"value\": false, \"answerBody\": \"a\"}, \"answer2\": {\"value\": true, \"answerBody\": \"v\"}, \"answer3\": {\"value\": false, \"answerBody\": \"s\"}}, \"coords\": {\"lat\": 51.25408078477207, \"lng\": 10.563828777343817}, \"iconePng\": {\"url\": null}, \"iconeSvg\": {\"url\": null}}], {\"id\": 67, \"lat\": null, \"lng\": null, \"city\": null, \"name\": \"Ecreatures Bidart\", \"prepa\": [{\"name\": \"Ecreatures Bidart\", \"zoom\": 7, \"teams\": [{\"mail\": \"mail@mail.com\", \"name\": \"Test\", \"color\": \"#5DD529\"}], \"viewport\": {\"lat\": 48.856614, \"lng\": 2.3522219}, \"expiration\": {\"day\": 11, \"year\": 2019, \"month\": 7}}, [{\"image\": {\"url\": \"07af678c18438f69d40704f7cafa0496.jpeg\"}, \"quizz\": {\"ask\": \"ask 1 ?\", \"answer1\": {\"value\": false, \"answerBody\": \"a\"}, \"answer2\": {\"value\": true, \"answerBody\": \"b\"}, \"answer3\": {\"value\": false, \"answerBody\": \"c\"}}, \"coords\": {\"lat\": 51.411956689602626, \"lng\": 5.449693035156315}, \"iconePng\": {\"url\": \"3e222d4aae19c872b14766270707a372.png\"}, \"iconeSvg\": {\"url\": null}}, {\"image\": {\"url\": null}, \"quizz\": {\"ask\": \"test\", \"answer1\": {\"value\": false, \"answerBody\": \"a\"}, \"answer2\": {\"value\": true, \"answerBody\": \"v\"}, \"answer3\": {\"value\": false, \"answerBody\": \"s\"}}, \"coords\": {\"lat\": 51.48727046131854, \"lng\": 5.707871746093815}, \"iconePng\": {\"url\": null}, \"iconeSvg\": {\"url\": null}}], null], \"hashLink\": \"YAdBXGw\", \"playable\": [{\"name\": \"Ecreatures Bidart\", \"zoom\": 7, \"teams\": [{\"mail\": \"mail@mail.com\", \"name\": \"Test\", \"color\": \"#5DD529\"}], \"viewport\": {\"lat\": 48.856614, \"lng\": 2.3522219}, \"expiration\": {\"day\": 11, \"year\": 2019, \"month\": 7}}, [{\"image\": {\"url\": \"07af678c18438f69d40704f7cafa0496.jpeg\"}, \"quizz\": {\"ask\": \"ask 1 ?\", \"answer1\": {\"value\": false, \"answerBody\": \"a\"}, \"answer2\": {\"value\": true, \"answerBody\": \"b\"}, \"answer3\": {\"value\": false, \"answerBody\": \"c\"}}, \"coords\": {\"lat\": 51.411956689602626, \"lng\": 5.449693035156315}, \"iconePng\": {\"url\": \"3e222d4aae19c872b14766270707a372.png\"}, \"iconeSvg\": {\"url\": null}}, {\"image\": {\"url\": null}, \"quizz\": {\"ask\": \"test\", \"answer1\": {\"value\": false, \"answerBody\": \"a\"}, \"answer2\": {\"value\": true, \"answerBody\": \"v\"}, \"answer3\": {\"value\": false, \"answerBody\": \"s\"}}, \"coords\": {\"lat\": 51.48727046131854, \"lng\": 5.707871746093815}, \"iconePng\": {\"url\": null}, \"iconeSvg\": {\"url\": null}}], null], \"createdAt\": \"2019-08-22T13:37:34.000Z\", \"deletedAt\": null, \"updatedAt\": \"2019-10-01T15:16:29.000Z\", \"id_manager\": 5, \"date_expiration\": \"2019-08-10\"}]', '[{\"name\": \"Ecreatures Bidart\", \"zoom\": 6, \"teams\": [{\"mail\": \"mail@mail.com\", \"name\": \"Test\", \"color\": \"#5DD529\"}], \"viewport\": {\"lat\": 51.4378319939697, \"lng\": 6.642769950632759}, \"expiration\": {\"day\": 11, \"year\": 2019, \"month\": 7}}, [{\"image\": {\"url\": \"07af678c18438f69d40704f7cafa0496.jpeg\"}, \"quizz\": {\"ask\": \"ask 1 ?\", \"answer1\": {\"value\": false, \"answerBody\": \"a\"}, \"answer2\": {\"value\": true, \"answerBody\": \"b\"}, \"answer3\": {\"value\": false, \"answerBody\": \"c\"}}, \"coords\": {\"lat\": 51.411956689602626, \"lng\": 5.449693035156315}, \"iconePng\": {\"url\": \"3e222d4aae19c872b14766270707a372.png\"}, \"iconeSvg\": {\"url\": null}}, {\"image\": {\"url\": null}, \"quizz\": {\"ask\": \"test\", \"answer1\": {\"value\": false, \"answerBody\": \"a\"}, \"answer2\": {\"value\": true, \"answerBody\": \"v\"}, \"answer3\": {\"value\": false, \"answerBody\": \"s\"}}, \"coords\": {\"lat\": 51.25408078477207, \"lng\": 10.563828777343817}, \"iconePng\": {\"url\": null}, \"iconeSvg\": {\"url\": null}}], {\"id\": 67, \"lat\": null, \"lng\": null, \"city\": null, \"name\": \"Ecreatures Bidart\", \"prepa\": [{\"name\": \"Ecreatures Bidart\", \"zoom\": 7, \"teams\": [{\"mail\": \"mail@mail.com\", \"name\": \"Test\", \"color\": \"#5DD529\"}], \"viewport\": {\"lat\": 48.856614, \"lng\": 2.3522219}, \"expiration\": {\"day\": 11, \"year\": 2019, \"month\": 7}}, [{\"image\": {\"url\": \"07af678c18438f69d40704f7cafa0496.jpeg\"}, \"quizz\": {\"ask\": \"ask 1 ?\", \"answer1\": {\"value\": false, \"answerBody\": \"a\"}, \"answer2\": {\"value\": true, \"answerBody\": \"b\"}, \"answer3\": {\"value\": false, \"answerBody\": \"c\"}}, \"coords\": {\"lat\": 51.411956689602626, \"lng\": 5.449693035156315}, \"iconePng\": {\"url\": \"3e222d4aae19c872b14766270707a372.png\"}, \"iconeSvg\": {\"url\": null}}, {\"image\": {\"url\": null}, \"quizz\": {\"ask\": \"test\", \"answer1\": {\"value\": false, \"answerBody\": \"a\"}, \"answer2\": {\"value\": true, \"answerBody\": \"v\"}, \"answer3\": {\"value\": false, \"answerBody\": \"s\"}}, \"coords\": {\"lat\": 51.48727046131854, \"lng\": 5.707871746093815}, \"iconePng\": {\"url\": null}, \"iconeSvg\": {\"url\": null}}], null], \"hashLink\": \"YAdBXGw\", \"playable\": [{\"name\": \"Ecreatures Bidart\", \"zoom\": 7, \"teams\": [{\"mail\": \"mail@mail.com\", \"name\": \"Test\", \"color\": \"#5DD529\"}], \"viewport\": {\"lat\": 48.856614, \"lng\": 2.3522219}, \"expiration\": {\"day\": 11, \"year\": 2019, \"month\": 7}}, [{\"image\": {\"url\": \"07af678c18438f69d40704f7cafa0496.jpeg\"}, \"quizz\": {\"ask\": \"ask 1 ?\", \"answer1\": {\"value\": false, \"answerBody\": \"a\"}, \"answer2\": {\"value\": true, \"answerBody\": \"b\"}, \"answer3\": {\"value\": false, \"answerBody\": \"c\"}}, \"coords\": {\"lat\": 51.411956689602626, \"lng\": 5.449693035156315}, \"iconePng\": {\"url\": \"3e222d4aae19c872b14766270707a372.png\"}, \"iconeSvg\": {\"url\": null}}, {\"image\": {\"url\": null}, \"quizz\": {\"ask\": \"test\", \"answer1\": {\"value\": false, \"answerBody\": \"a\"}, \"answer2\": {\"value\": true, \"answerBody\": \"v\"}, \"answer3\": {\"value\": false, \"answerBody\": \"s\"}}, \"coords\": {\"lat\": 51.48727046131854, \"lng\": 5.707871746093815}, \"iconePng\": {\"url\": null}, \"iconeSvg\": {\"url\": null}}], null], \"createdAt\": \"2019-08-22T13:37:34.000Z\", \"deletedAt\": null, \"updatedAt\": \"2019-10-01T15:16:29.000Z\", \"id_manager\": 5, \"date_expiration\": \"2019-08-10\"}]', '2019-08-22 15:37:34', '2019-10-02 12:10:18', NULL),
(158, 5, 'test', '2019-09-30', NULL, 'bWpyKZM', NULL, NULL, '[{\"name\": \"test\", \"zoom\": 1, \"teams\": null, \"viewport\": {\"lat\": 48.856614, \"lng\": 2.3522219}, \"expiration\": {\"day\": 31, \"year\": 2019, \"month\": 8}}, [{\"quizz\": {\"ask\": \"test\", \"answer1\": {\"value\": false, \"answerBody\": \"test\"}, \"answer2\": {\"value\": true, \"answerBody\": \"test\"}, \"answer3\": {\"value\": false, \"answerBody\": \"test\"}}, \"coords\": {\"lat\": 51.54538239912754, \"lng\": 8.811509441406315}}], null]', NULL, '2019-08-28 15:36:13', '2019-10-01 10:17:27', '2019-10-01 10:17:27'),
(159, 5, 'test', '2019-09-30', NULL, 'ETgOQZg', NULL, NULL, '[{\"name\": \"test\", \"zoom\": 5, \"teams\": null, \"viewport\": {\"lat\": 48.856614, \"lng\": 2.3522219}, \"expiration\": {\"day\": 31, \"year\": 2019, \"month\": 8}}, [], null]', NULL, '2019-08-31 15:41:02', '2019-10-01 10:17:23', '2019-10-01 10:17:23');

-- --------------------------------------------------------

--
-- Structure de la table `rallymanagers`
--

CREATE TABLE `rallymanagers` (
  `id` int(3) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `tel` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `deletedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `rallymanagers`
--

INSERT INTO `rallymanagers` (`id`, `mail`, `tel`, `password`, `name`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(3, 'alain@email.com', 79889, '$2a$08$4HWs8xrxT3Orsv1hjtrl7O5kI3YoIWqmfuW/XSNy8mF8CHO1ETBwu', 'Alain', '2019-07-03 08:33:17', '2019-07-03 08:33:17', NULL),
(5, 'tony@email.com', 12355, '$2a$08$bpoXEiWGTdsH8Lcu4QCZn.YDLu9DuF2dZOB7HvmBtHLBrZa3dCG0y', 'tony', '2019-07-10 06:30:25', '2019-07-10 06:30:25', NULL),
(16, 'michelperrin@icloud.com', 755, '$2a$08$YTbQPB8YA2.yyeMSz3rno.MXIwnzo.lEfg6KJfjmTj1Z8aeJIiewO', 'Michel', '2019-07-12 08:51:34', '2019-07-12 08:51:34', NULL),
(17, 'test@email.com', 7256, '$2a$08$HbVGZdxnWUy9XS5jb9dcmeGmDqB68MngLL916Y5Aqkqn0bE8hp5aG', 'testeur', '2019-08-19 15:31:06', '2019-08-19 15:31:06', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL,
  `id_rally` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `deletedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `waypoints`
--

CREATE TABLE `waypoints` (
  `id` int(11) NOT NULL,
  `id_rally` int(11) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `title` varchar(255) NOT NULL,
  `iconeSvg` varchar(255) DEFAULT NULL,
  `iconePng` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `question` text NOT NULL,
  `reponse1` varchar(255) NOT NULL,
  `reponse2` varchar(255) NOT NULL,
  `reponse3` varchar(255) NOT NULL,
  `valrep1` tinyint(1) NOT NULL DEFAULT '0',
  `valrep2` tinyint(1) NOT NULL DEFAULT '0',
  `valrep3` tinyint(1) NOT NULL DEFAULT '0',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `deletedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `rallyes`
--
ALTER TABLE `rallyes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_manager` (`id_manager`);

--
-- Index pour la table `rallymanagers`
--
ALTER TABLE `rallymanagers`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_rally` (`id_rally`);

--
-- Index pour la table `waypoints`
--
ALTER TABLE `waypoints`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_rally` (`id_rally`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `rallyes`
--
ALTER TABLE `rallyes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;

--
-- AUTO_INCREMENT pour la table `rallymanagers`
--
ALTER TABLE `rallymanagers`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `waypoints`
--
ALTER TABLE `waypoints`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `rallyes`
--
ALTER TABLE `rallyes`
  ADD CONSTRAINT `rallyes_ibfk_1` FOREIGN KEY (`id_manager`) REFERENCES `rallymanagers` (`id`);

--
-- Contraintes pour la table `teams`
--
ALTER TABLE `teams`
  ADD CONSTRAINT `teams_ibfk_1` FOREIGN KEY (`id_rally`) REFERENCES `rallyes` (`id`);

--
-- Contraintes pour la table `waypoints`
--
ALTER TABLE `waypoints`
  ADD CONSTRAINT `waypoints_ibfk_1` FOREIGN KEY (`id_rally`) REFERENCES `rallyes` (`id`);